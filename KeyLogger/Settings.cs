﻿using System;

namespace KeyLogger.Properties
{
	// Этот класс позволяет обрабатывать определенные события в классе параметров:
	//  Событие SettingChanging возникает перед изменением значения параметра.
	//  Событие PropertyChanged возникает после изменения значения параметра.
	//  Событие SettingsLoaded возникает после загрузки значений параметров.
	//  Событие SettingsSaving возникает перед сохранением значений параметров.
	internal sealed partial class Settings
	{

		public Settings()
		{
			this.SettingsLoaded += Settings_SettingsLoaded;
		}

		void Settings_SettingsLoaded(object sender, System.Configuration.SettingsLoadedEventArgs e)
		{
			String[] path = this.LogFileName.Split('\\');

			if (path.Length == 1)
				this.LogFileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + '\\' + this.LogFileName;
		}
	}
}
