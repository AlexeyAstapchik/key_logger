﻿namespace KeyLogger.GUI
{
	partial class FormSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.textBoxLogFileName = new System.Windows.Forms.TextBox();
            this.textBoxTimeStamp = new System.Windows.Forms.TextBox();
            this.buttonChangeLogFileName = new System.Windows.Forms.Button();
            this.labelLogFileName = new System.Windows.Forms.Label();
            this.labelTimeStamp = new System.Windows.Forms.Label();
            this.checkBoxAutoStart = new System.Windows.Forms.CheckBox();
            this.checkBoxSilentMode = new System.Windows.Forms.CheckBox();
            this.radioButtonAutoRunHKLM = new System.Windows.Forms.RadioButton();
            this.radioButtonAutoRunHKCU = new System.Windows.Forms.RadioButton();
            this.radioButtonAutoRunNull = new System.Windows.Forms.RadioButton();
            this.labelAutoRun = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(35, 116);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 0;
            this.buttonSave.Text = global::KeyLogger.Properties.Resources.ButtonSave;
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(150, 116);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 1;
            this.buttonReset.Text = global::KeyLogger.Properties.Resources.ButtonReset;
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // textBoxLogFileName
            // 
            this.textBoxLogFileName.Location = new System.Drawing.Point(12, 48);
            this.textBoxLogFileName.Name = "textBoxLogFileName";
            this.textBoxLogFileName.Size = new System.Drawing.Size(159, 20);
            this.textBoxLogFileName.TabIndex = 2;
            // 
            // textBoxTimeStamp
            // 
            this.textBoxTimeStamp.Location = new System.Drawing.Point(12, 87);
            this.textBoxTimeStamp.Name = "textBoxTimeStamp";
            this.textBoxTimeStamp.Size = new System.Drawing.Size(159, 20);
            this.textBoxTimeStamp.TabIndex = 3;
            // 
            // buttonChangeLogFileName
            // 
            this.buttonChangeLogFileName.Location = new System.Drawing.Point(177, 46);
            this.buttonChangeLogFileName.Name = "buttonChangeLogFileName";
            this.buttonChangeLogFileName.Size = new System.Drawing.Size(75, 23);
            this.buttonChangeLogFileName.TabIndex = 4;
            this.buttonChangeLogFileName.Text = global::KeyLogger.Properties.Resources.ButtonChangeLogFileName;
            this.buttonChangeLogFileName.UseVisualStyleBackColor = true;
            this.buttonChangeLogFileName.Click += new System.EventHandler(this.buttonChangeLogFileName_Click);
            // 
            // labelLogFileName
            // 
            this.labelLogFileName.AutoSize = true;
            this.labelLogFileName.Location = new System.Drawing.Point(12, 32);
            this.labelLogFileName.Name = "labelLogFileName";
            this.labelLogFileName.Size = new System.Drawing.Size(0, 13);
            this.labelLogFileName.TabIndex = 5;
            // 
            // labelTimeStamp
            // 
            this.labelTimeStamp.AutoSize = true;
            this.labelTimeStamp.Location = new System.Drawing.Point(12, 71);
            this.labelTimeStamp.Name = "labelTimeStamp";
            this.labelTimeStamp.Size = new System.Drawing.Size(0, 13);
            this.labelTimeStamp.TabIndex = 6;
            // 
            // checkBoxAutoStart
            // 
            this.checkBoxAutoStart.AutoSize = true;
            this.checkBoxAutoStart.Location = new System.Drawing.Point(12, 12);
            this.checkBoxAutoStart.Name = "checkBoxAutoStart";
            this.checkBoxAutoStart.Size = new System.Drawing.Size(135, 17);
            this.checkBoxAutoStart.TabIndex = 7;
            this.checkBoxAutoStart.Text = global::KeyLogger.Properties.Resources.CheckBoxAutoStart;
            this.checkBoxAutoStart.UseVisualStyleBackColor = true;
            this.checkBoxAutoStart.Visible = false;
            // 
            // checkBoxSilentMode
            // 
            this.checkBoxSilentMode.AutoSize = true;
            this.checkBoxSilentMode.Location = new System.Drawing.Point(258, 10);
            this.checkBoxSilentMode.Name = "checkBoxSilentMode";
            this.checkBoxSilentMode.Size = new System.Drawing.Size(81, 17);
            this.checkBoxSilentMode.TabIndex = 8;
            this.checkBoxSilentMode.Text = global::KeyLogger.Properties.Resources.CheckBoxSilentMode;
            this.checkBoxSilentMode.UseVisualStyleBackColor = true;
            this.checkBoxSilentMode.Visible = false;
            this.checkBoxSilentMode.CheckedChanged += new System.EventHandler(this.checkBoxSilentMode_CheckedChanged);
            // 
            // radioButtonAutoRunHKLM
            // 
            this.radioButtonAutoRunHKLM.AutoSize = true;
            this.radioButtonAutoRunHKLM.Location = new System.Drawing.Point(258, 65);
            this.radioButtonAutoRunHKLM.Name = "radioButtonAutoRunHKLM";
            this.radioButtonAutoRunHKLM.Size = new System.Drawing.Size(81, 17);
            this.radioButtonAutoRunHKLM.TabIndex = 9;
            this.radioButtonAutoRunHKLM.TabStop = true;
            this.radioButtonAutoRunHKLM.Text = global::KeyLogger.Properties.Resources.RadioButtonAutoRunHKLM;
            this.radioButtonAutoRunHKLM.UseVisualStyleBackColor = true;
            // 
            // radioButtonAutoRunHKCU
            // 
            this.radioButtonAutoRunHKCU.AutoSize = true;
            this.radioButtonAutoRunHKCU.Location = new System.Drawing.Point(258, 46);
            this.radioButtonAutoRunHKCU.Name = "radioButtonAutoRunHKCU";
            this.radioButtonAutoRunHKCU.Size = new System.Drawing.Size(99, 17);
            this.radioButtonAutoRunHKCU.TabIndex = 10;
            this.radioButtonAutoRunHKCU.TabStop = true;
            this.radioButtonAutoRunHKCU.Text = global::KeyLogger.Properties.Resources.RadioButtonAutoRunHKCU;
            this.radioButtonAutoRunHKCU.UseVisualStyleBackColor = true;
            this.radioButtonAutoRunHKCU.CheckedChanged += new System.EventHandler(this.radioButtonAutoRunHKCU_CheckedChanged);
            // 
            // radioButtonAutoRunNull
            // 
            this.radioButtonAutoRunNull.AutoSize = true;
            this.radioButtonAutoRunNull.Location = new System.Drawing.Point(258, 85);
            this.radioButtonAutoRunNull.Name = "radioButtonAutoRunNull";
            this.radioButtonAutoRunNull.Size = new System.Drawing.Size(78, 17);
            this.radioButtonAutoRunNull.TabIndex = 11;
            this.radioButtonAutoRunNull.TabStop = true;
            this.radioButtonAutoRunNull.Text = global::KeyLogger.Properties.Resources.RadioButtonAutoRunNull;
            this.radioButtonAutoRunNull.UseVisualStyleBackColor = true;
            // 
            // labelAutoRun
            // 
            this.labelAutoRun.AutoSize = true;
            this.labelAutoRun.Location = new System.Drawing.Point(255, 30);
            this.labelAutoRun.Name = "labelAutoRun";
            this.labelAutoRun.Size = new System.Drawing.Size(0, 13);
            this.labelAutoRun.TabIndex = 12;
            // 
            // FormSettings
            // 
            this.AcceptButton = this.buttonSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 162);
            this.Controls.Add(this.labelAutoRun);
            this.Controls.Add(this.radioButtonAutoRunNull);
            this.Controls.Add(this.radioButtonAutoRunHKCU);
            this.Controls.Add(this.radioButtonAutoRunHKLM);
            this.Controls.Add(this.checkBoxSilentMode);
            this.Controls.Add(this.checkBoxAutoStart);
            this.Controls.Add(this.labelTimeStamp);
            this.Controls.Add(this.labelLogFileName);
            this.Controls.Add(this.buttonChangeLogFileName);
            this.Controls.Add(this.textBoxTimeStamp);
            this.Controls.Add(this.textBoxLogFileName);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormSettings";
            this.Load += new System.EventHandler(this.FormSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.TextBox textBoxLogFileName;
		private System.Windows.Forms.TextBox textBoxTimeStamp;
		private System.Windows.Forms.Button buttonChangeLogFileName;
		private System.Windows.Forms.Label labelLogFileName;
		private System.Windows.Forms.Label labelTimeStamp;
		private System.Windows.Forms.CheckBox checkBoxAutoStart;
		private System.Windows.Forms.CheckBox checkBoxSilentMode;
		private System.Windows.Forms.RadioButton radioButtonAutoRunHKLM;
		private System.Windows.Forms.RadioButton radioButtonAutoRunHKCU;
		private System.Windows.Forms.RadioButton radioButtonAutoRunNull;
		private System.Windows.Forms.Label labelAutoRun;

	}
}