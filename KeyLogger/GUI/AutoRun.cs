﻿using KeyLogger.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace KeyLogger.GUI
{
	class AutoRun
	{
		public enum HK { HKCU, HKLM, HKCUSM, HKLMSM, NULL };

		private String keyName;
		private String valueName;
		private String silentModeArg;
		private String fileName;
		private Boolean asAdmin;
		private HK autoRunState;

		public HK AutoRunState
		{
			get { return this.autoRunState; }
		}

		public AutoRun(Boolean asAdmin)
		{
			this.keyName = "Software\\Microsoft\\Windows\\CurrentVersion\\Run\\";
			this.valueName = "KeyLogger";
			this.silentModeArg = " /S";
			this.fileName = "\"" + Process.GetCurrentProcess().MainModule.FileName + "\"";
			this.asAdmin = asAdmin;

			String HKCUValue = Registry.CurrentUser.OpenSubKey(this.keyName).GetValue(this.valueName) as String;
			String HKLMValue = Registry.LocalMachine.OpenSubKey(this.keyName).GetValue(this.valueName) as String;

			this.autoRunState = this.GetAutoRunState();
		}

		public HK GetAutoRunState()
		{
			String HKCUValue = Registry.CurrentUser.OpenSubKey(this.keyName).GetValue(this.valueName) as String;
			String HKLMValue = Registry.LocalMachine.OpenSubKey(this.keyName).GetValue(this.valueName) as String;

			if (HKCUValue == this.fileName)
			{
				return HK.HKCU;
			}
			if (HKLMValue == this.fileName)
			{
				return HK.HKLM;
			}
			if (HKCUValue == this.fileName + this.silentModeArg)
			{
				return HK.HKCUSM;
			}
			if (HKLMValue == this.fileName + this.silentModeArg)
			{
				return HK.HKLMSM;
			}
			else
			{
				return HK.NULL;
			}
		}

		public void SetAutoRunState(HK autoRunState)
		{
			if (this.autoRunState != autoRunState)
			{
				if (this.asAdmin)
				{
					switch (autoRunState)
					{
						case AutoRun.HK.HKCU:
							{
								Registry.CurrentUser.OpenSubKey(this.keyName, true).SetValue(this.valueName, this.fileName);
								Registry.LocalMachine.OpenSubKey(this.keyName, true).DeleteValue(this.valueName, false);
							}
							break;
						case AutoRun.HK.HKLM:
							{
								Registry.CurrentUser.OpenSubKey(this.keyName, true).DeleteValue(this.valueName, false);
								Registry.LocalMachine.OpenSubKey(this.keyName, true).SetValue(this.valueName, this.fileName);
							}
							break;
						case AutoRun.HK.HKCUSM:
							{
								Registry.CurrentUser.OpenSubKey(this.keyName, true).SetValue(this.valueName, this.fileName + this.silentModeArg);
								Registry.LocalMachine.OpenSubKey(this.keyName, true).DeleteValue(this.valueName, false);
							}
							break;
						case AutoRun.HK.HKLMSM:
							{
								Registry.CurrentUser.OpenSubKey(this.keyName, true).DeleteValue(this.valueName, false);
								Registry.LocalMachine.OpenSubKey(this.keyName, true).SetValue(this.valueName, this.fileName + this.silentModeArg);
							}
							break;
						case AutoRun.HK.NULL:
							{
								Registry.CurrentUser.OpenSubKey(this.keyName, true).DeleteValue(this.valueName, false);
								Registry.LocalMachine.OpenSubKey(this.keyName, true).DeleteValue(this.valueName, false);
							}
							break;
					}
				}
				else
				{
					switch (autoRunState)
					{
						case AutoRun.HK.HKCU:
							{
								if (this.autoRunState != HK.HKLM && this.autoRunState != HK.HKLMSM)
								{
									Registry.CurrentUser.OpenSubKey(this.keyName, true).SetValue(this.valueName, this.fileName);
								}
								else
								{
									throw new InvalidOperationException(Resources.HKInvalidOperationException);
								}
							}
							break;
						case AutoRun.HK.HKLM:
							{
								throw new SecurityException(Resources.HKSecurityException);
							}
						case AutoRun.HK.HKCUSM:
							{
								if (this.autoRunState != HK.HKLM && this.autoRunState != HK.HKLMSM)
								{
									Registry.CurrentUser.OpenSubKey(this.keyName, true).SetValue(this.valueName, this.fileName + this.silentModeArg);
								}
								else
								{
									throw new InvalidOperationException(Resources.HKInvalidOperationException);
								}
							}
							break;
						case AutoRun.HK.HKLMSM:
							{
								throw new SecurityException(Resources.HKSecurityException);
							}
						case AutoRun.HK.NULL:
							{
								if (this.autoRunState != HK.HKLM && this.autoRunState != HK.HKLMSM)
								{
									Registry.CurrentUser.OpenSubKey(this.keyName, true).DeleteValue(this.valueName, false);
								}
								else
								{
									throw new SecurityException(Resources.HKSecurityException);
								}
							}
							break;
					}
				}
			}
		}
	}
}
