﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Text;
using System.Windows.Input;

namespace Listeners
{
	namespace Keyboard
	{
		public delegate void RawKeyEventHandler(object sender, RawKeyEventArgs args);

		public delegate void RawKeyPressEventHandler(object sender, RawKeyPressEventArgs args);

		public class RawKeyEventArgs : EventArgs
		{

			private Int32 _VKCode;
			public Int32 VKCode
			{
				get { return this._VKCode; }
			}

			private Key _Key;
			public Key Key
			{
				get { return this._Key; }
			}

			private Boolean _IsSysKey;
			public Boolean IsSysKey
			{
				get { return this._IsSysKey; }
			}

			public RawKeyEventArgs(Int32 VKCode, Boolean IsSysKey)
			{
				this._VKCode = VKCode;
				this._IsSysKey = IsSysKey;
				this._Key = KeyInterop.KeyFromVirtualKey(VKCode);
			}
		}


		public class RawKeyPressEventArgs : EventArgs
		{

			private Int32 _VKCode;
			public Int32 VKCode
			{
				get { return this._VKCode; }
			}


			private Key _Key;
			public Key Key
			{
				get { return this._Key; }
			}

			private Char _Character;
			public Char Character
			{
				get { return this._Character; }
			}

			private Process _ActiveProcess;
			public Process ActiveProcess
			{
				get { return this._ActiveProcess; }
			}

			public RawKeyPressEventArgs(Int32 VKCode, Char Character, Process ActiveProcess)
			{
				this._VKCode = VKCode;
				this._Character = Character;
				this._Key = KeyInterop.KeyFromVirtualKey(VKCode);
				this._ActiveProcess = ActiveProcess;
			}
		}

		public class KeyboardListener
		{
			#region Windows structure definitions

			[StructLayout(LayoutKind.Sequential)]
			private struct KeyboardHookStruct
			{
			
				public Int32 vkCode;
				
				public Int32 scanCode;
			
				public Int32 flags;
		
				public Int32 time;
	
				public Int32 dwExtraInfo;
			}

			#endregion

			#region Windows function imports

			[DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
			private static extern Int32 SetWindowsHookEx(Int32 idHook, HookProc lpfn, IntPtr hMod, Int32 dwThreadId);

			[DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
			private static extern Int32 UnhookWindowsHookEx(Int32 idHook);

			[DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
			private static extern Int32 CallNextHookEx(Int32 idHook, Int32 nCode, Int32 wParam, IntPtr lParam);

			private delegate Int32 HookProc(Int32 nCode, Int32 wParam, IntPtr lParam);

			[DllImport("user32")]
			private static extern Int32 ToUnicodeEx(Int32 uVirtKey, Int32 uScanCode, Byte[] lpbKeyState, Byte[] pwszBuff, Int32 cchBuff, UInt32 wFlags, IntPtr dwhkl);

			[DllImport("user32.dll", CharSet = CharSet.Auto)]
			private static extern Int32 GetWindowThreadProcessId(IntPtr handleWindow, out Int32 lpdwProcessID);

			[DllImport("user32.dll", CharSet = CharSet.Auto)]
			private static extern IntPtr GetKeyboardLayout(Int32 WindowsThreadProcessID);

			[DllImport("user32.dll", CharSet = CharSet.Auto)]
			public static extern IntPtr GetForegroundWindow();

			[DllImport("user32")]
			private static extern Int32 GetKeyboardState(Byte[] pbKeyState);

			[DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
			private static extern Int16 GetKeyState(Int32 vKey);

			#endregion

			#region Windows constants

			private const Int32 WH_KEYBOARD_LL = 13;
			//private const Int32 WH_KEYBOARD = 2;
			private const Int32 WM_KEYDOWN = 0x100;
			private const Int32 WM_KEYUP = 0x101;
			private const Int32 WM_SYSKEYDOWN = 0x104;
			private const Int32 WM_SYSKEYUP = 0x105;
			private const Byte VK_SHIFT = 0x10;
			private const Byte VK_CAPITAL = 0x14;
			private const Byte VK_NUMLOCK = 0x90;

			#endregion

			#region Events

			public event RawKeyEventHandler KeyDown;

			public event RawKeyPressEventHandler KeyPress;

			public event RawKeyEventHandler KeyUp;

			#endregion

			~KeyboardListener()
			{
				Stop(false);
			}
			private Int32 hKeyboardHook = 0;

			private static HookProc KeyboardHookProcedure;

			public Boolean Start()
			{
				if (hKeyboardHook == 0)
				{
					KeyboardHookProcedure = new HookProc(KeyboardHookProc);

					hKeyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardHookProcedure, Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]), 0);

					if (hKeyboardHook == 0)
					{
						Stop(false);
						throw new Win32Exception(Marshal.GetLastWin32Error());
					}
					else
					{
						return true;
					}
				}
				else
				{
					return false;
				}
			}

			public Boolean Stop()
			{
				return this.Stop(true);
			}
			private Boolean Stop(Boolean ThrowExceptions)
			{
				if (hKeyboardHook != 0)
				{
					Int32 retKeyboard = UnhookWindowsHookEx(hKeyboardHook);
					hKeyboardHook = 0;
					if (retKeyboard == 0 && ThrowExceptions)
					{
						throw new Win32Exception(Marshal.GetLastWin32Error());
					}
					else
					{
						return true;
					}
				}
				else
				{
					return false;
				}
			}

			/// <summary>
			/// A callback function which will be called every time a keyboard activity detected.
			/// </summary>
			/// <param name="nCode">
			/// [in] Specifies whether the hook procedure must process the message. 
			/// If nCode is HC_ACTION, the hook procedure must process the message. 
			/// If nCode is less than zero, the hook procedure must pass the message to the 
			/// CallNextHookEx function without further processing and must return the 
			/// value returned by CallNextHookEx.
			/// </param>
			/// <param name="wParam">
			/// [in] Specifies whether the message was sent by the current thread. 
			/// If the message was sent by the current thread, it is nonzero; otherwise, it is zero. 
			/// </param>
			/// <param name="lParam">
			/// [in] Pointer to a CWPSTRUCT structure that contains details about the message. 
			/// </param>
			/// <returns>
			/// If nCode is less than zero, the hook procedure must return the value returned by CallNextHookEx. 
			/// If nCode is greater than or equal to zero, it is highly recommended that you call CallNextHookEx 
			/// and return the value it returns; otherwise, other applications that have installed WH_CALLWNDPROC 
			/// hooks will not receive hook notifications and may behave incorrectly as a result. If the hook 
			/// procedure does not call CallNextHookEx, the return value should be zero. 
			/// </returns>
			private Int32 KeyboardHookProc(Int32 nCode, Int32 wParam, IntPtr lParam)
			{
				
                if ((nCode >= 0) && (KeyDown != null || KeyUp != null || KeyPress != null))
				{
					KeyboardHookStruct MyKeyboardHookStruct = (KeyboardHookStruct)Marshal.PtrToStructure(lParam, typeof(KeyboardHookStruct));

					if (KeyDown != null)
					{
						switch (wParam)
						{
							case WM_KEYDOWN:
								KeyDown(this, new RawKeyEventArgs(MyKeyboardHookStruct.vkCode, false));
								break;
							case WM_SYSKEYDOWN:
								KeyDown(this, new RawKeyEventArgs(MyKeyboardHookStruct.vkCode, true));
								break;
						}
					}

					if (KeyPress != null && wParam == WM_KEYDOWN)
					{
						GetKeyState(VK_SHIFT);
						GetKeyState(VK_CAPITAL);

						Byte[] keyState = new Byte[256];
						GetKeyboardState(keyState);
						Byte[] inBuffer = new Byte[2];

						Int32 processId;
						Int32 WinThreadProcId = GetWindowThreadProcessId(GetForegroundWindow(), out processId);
						IntPtr KeybLayout = GetKeyboardLayout(WinThreadProcId);

						if (ToUnicodeEx(MyKeyboardHookStruct.vkCode, MyKeyboardHookStruct.scanCode, keyState, inBuffer, inBuffer.Length, 0, KeybLayout) >= 1)
						{
							Process activeProcess = Process.GetProcessById(processId);
							foreach (Char key in Encoding.Unicode.GetString(inBuffer))
							{
								KeyPress(this, new RawKeyPressEventArgs(MyKeyboardHookStruct.vkCode, key, activeProcess));
							}
						}
					}

					if (KeyUp != null)
					{
						switch (wParam)
						{
							case WM_KEYUP:
								KeyDown(this, new RawKeyEventArgs(MyKeyboardHookStruct.vkCode, false));
								break;
							case WM_SYSKEYUP:
								KeyDown(this, new RawKeyEventArgs(MyKeyboardHookStruct.vkCode, true));
								break;
						}
					}
				}

				return CallNextHookEx(hKeyboardHook, nCode, wParam, lParam);
			}
		}
	}
}